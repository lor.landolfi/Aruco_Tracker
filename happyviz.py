import sys
import cv2
import numpy as np
from PyQt5.QtWidgets import QApplication, QLabel, QVBoxLayout, QWidget, QSlider, QHBoxLayout
from PyQt5.QtCore import Qt, QThread, pyqtSignal
from multiprocessing import Queue,Process
from PyQt5.QtGui import QImage, QPixmap, QFont
import threading


class SliderUpdater(threading.Thread):
    def __init__(self, queue, slider):
        super().__init__()
        self.queue = queue
        self.slider = slider
        self.running = True


    def run(self):
        while self.running:
            try:
                new_value = self.queue.get(timeout=1)
                self.slider.setValue(new_value)
            except queue.Empty:
                pass

    def stop(self):
        self.running = False



class DistanceWidget(QWidget):
    def __init__(self, queue):
        super().__init__()
        self.queue = queue
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()

        # Label to display the distance value
        self.distance_label = QLabel('Distance: 0')
        layout.addWidget(self.distance_label)

        # Slider to represent the distance
        self.slider = QSlider(Qt.Horizontal)
        self.slider.setMinimum(0)
        self.slider.setMaximum(100)
        self.slider.setValue(0)
        self.slider.setTickPosition(QSlider.TicksBelow)
        self.slider.setTickInterval(10)
        layout.addWidget(self.slider)

        self.setLayout(layout)
        self.setWindowTitle('Distance Widget')

    def updateDistanceLabel(self, value):
        self.distance_label.setText(f'Distance: {value}')



class VideoThread(QThread):
    change_pixmap_signal = pyqtSignal(np.ndarray)

    def __init__(self, queue):
        super().__init__()
        self.queue = queue
        self.running = True

    def run(self):
        while self.running:
            if not self.queue.empty():
                frame = self.queue.get()
                self.change_pixmap_signal.emit(frame)
        self.queue.close()

    def stop(self):
        self.running = False

class VideoWidget(QWidget):
    def __init__(self, queue,name):
        super().__init__()
        self.queue = queue
        self.name = name
        self.init_ui()

    def init_ui(self):
        self.setWindowTitle(self.name)
        self.video_label = QLabel()
        layout = QVBoxLayout()
        layout.addWidget(self.video_label)
        self.setLayout(layout)

    def show_frame(self, frame):
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image = QImage(frame, frame.shape[1], frame.shape[0], QImage.Format_RGB888)
        pixmap = QPixmap.fromImage(image)
        self.video_label.setPixmap(pixmap)
        self.video_label.setAlignment(Qt.AlignCenter)

    def closeEvent(self, event):
        self.video_thread.stop()
        event.accept()


class ConcatenatedWidgets(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()
        self.setLayout(layout)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    queue = Queue()

    video_capture = cv2.VideoCapture(0)
    if not video_capture.isOpened():
        print("Error: Could not open video device.")
        sys.exit()

    def read_frame(queue):
        while True:
            ret, frame = video_capture.read()
            if not ret:
                break
            if not queue.full():
                queue.put(frame)

    video_thread = VideoThread(queue)
    video_widget = VideoWidget(queue)
    video_thread.change_pixmap_signal.connect(video_widget.show_frame)
    video_thread.start()

    read_frame_process = Process(target=read_frame, args=(queue,))
    read_frame_process.start()

    video_widget.show()
    sys.exit(app.exec_())

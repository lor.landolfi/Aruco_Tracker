"""
Framework   : OpenCV Aruco
Description : Calibration of camera and using that for finding pose of multiple markers
Status      : Working
References  :
    1) https://docs.opencv.org/3.4.0/d5/dae/tutorial_aruco_detection.html
    2) https://docs.opencv.org/3.4.3/dc/dbb/tutorial_py_calibration.html
    3) https://docs.opencv.org/3.1.0/d5/dae/tutorial_aruco_detection.html
"""

import numpy as np
import cv2
import cv2.aruco as aruco
import glob
from matplotlib import pyplot as plt
import streamlit as st
from scipy.spatial import ConvexHull
from enum import Enum
import websocket
import json
import argparse
import copy
from happyviz import *



###------------------ ARUCO TRACKER ---------------------------

class Modality(Enum):
    LINE = 2
    TRIANGLE = 3
    RECTANGLE = 4
    PENTAGON = 5
    EXAGON = 6


class GeomComputer:

    points = []
    mmul = {"m" : 1, "dm" : 10 , "cm" : 100 , "mm" : 1000}

    def __init__(self,points, rotations,unit="m"):
        self.points = points
        self.rot = rotations
        self.unit = unit

    def check(self):
        return False

    def display(self,frame,mtx,dist):
        plines = []
        if self.check():
            for i in range(0, len(self.points)):
                #draw axis for the aruco markers
                p2d = cv2.projectPoints((0,0,0) ,self.rot[i], self.points[i], mtx, dist)
                plines.append( (int(p2d[0][0][0][0]), int(p2d[0][0][0][1])))
                cv2.circle(frame, (int(p2d[0][0][0][0]), int(p2d[0][0][0][1])), 20, (255,255,255) , 3)
                #cv2.drawFrameAxes(frame, mtx, dist, self.rot[i], self.points[i], 0.1)

            for i in range(0,len(plines)):
                cv2.line(frame, plines[i] , plines[(i+1)%(len(plines))], (0,255,0) , thickness=3)

            apoints = np.array(self.points)
            apoints = apoints.reshape((apoints.shape[0] , apoints.shape[2]))


    def Perimeter(self):
        if self.check():
            hull = ConvexHull(np.array(self.points))
            return hull.area

    def Area(self):
        if self.check():
            hull = ConvexHull(np.array(self.points))
            return hull.volume


class LineComputer(GeomComputer):

    def __init__(self, points, rotations):
        super().__init__(points, rotations)

    def check(self):
        return len(self.points) == 2

    def Perimeter(self):
        if self.check():
            return np.linalg.norm(self.points[0] - self.points[1])


class TriangleComputer(GeomComputer):

    def __init__(self, points, rotations):
        super().__init__(points, rotations)

    def check(self):
        return len(self.points) == 3

    def Perimeter(self):
        if self.check():

            P = np.linalg.norm(self.points[0] - self.points[1]) + np.linalg.norm(self.points[0] - self.points[2]) + np.linalg.norm(self.points[1] - self.points[2])

            return round(P * GeomComputer.mmul[self.unit],3)



    def Area(self):
        if self.check():
            SP = self.Perimeter() /2.0

            l1 = np.linalg.norm(self.points[0] - self.points[1])
            l2 = np.linalg.norm(self.points[1] - self.points[2])
            l3 = np.linalg.norm(self.points[2] - self.points[0])

            A = np.sqrt( (SP) * (SP - l1) * (SP - l2) * (SP - l3)   )

            return round(A * GeomComputer.mmul[self.unit]**2,3)

        else:
            return None



class PyramidComputer(GeomComputer):

    def __init__(self, points, rotations):
        super().__init__(points, rotations)

    def check(self):
        return len(self.points) == 3

    def Perimeter(self):
        if self.check():
            self.points.append(np.array([[0,0,0.5]]))

            apoints = np.array(self.points)
            apoints = apoints.reshape((apoints.shape[0] , apoints.shape[2]))
            hull = ConvexHull(apoints)

            return hull.area

def makeFeedback(frame,args,oargs={},pargs={"area" : 0.01},thresh=0.003):

    nframe = np.ones(frame.shape,dtype=np.uint8)
    
    oar = oargs.get('area',-10000)
    if oar == None:
        oar = -100000

    diff = np.abs(oar - pargs["area"])
    vdiff = oar - pargs["area"]
    maxdiff = 0.03
    
    cg = 255 - ((diff / maxdiff) * 255)
    cr = (diff / maxdiff) * 255

    diffcol = (cr,cg,cr)

    for i in range(0,3):
        nframe[:,:,i] = nframe[:,:,i] * diffcol[i]

    sput = ""
    if diff < thresh:
        sput = "Fuoco!"
    else:

        if diff < maxdiff / 3.0:
            sput = "Fuochino..."
        else:
            sput = "Acqua"

        if vdiff < 0 and oar > 0:
            sput = sput + " allontana i punti"
        if vdiff > 0 and oar > 0:
            sput = sput + " avvicina i punti"

   

    # font 
    font = cv2.FONT_HERSHEY_SIMPLEX 
    
    # org 
    org = (50, 50) 
    
    # fontScale 
    fontScale = 1
    
    # Blue color in BGR 
    color = (255, 0, 0) 
    
    # Line thickness of 2 px 
    thickness = 2
    
    # Using cv2.putText() method 
    nframe = cv2.putText(nframe, sput, org, font,  
                    fontScale, color, thickness, cv2.LINE_AA) 

    return nframe





def readFrame(args,queues):
    while (True):
        ret, frame = cap.read()
        #if ret returns false, there is likely a problem with the webcam/camera.
        #In that case uncomment the below line, which will replace the empty frame 
        #with a test image used in the opencv docs for aruco at https://www.docs.opencv.org/4.5.3/singlemarkersoriginal.jpg
        # frame = cv2.imread('./images/test image.jpg') 

        # operations on the frame
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # set dictionary size depending on the aruco marker selected
        #aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
        aruco_dict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_250)

        # detector parameters can be set here (List of detection parameters[3])
        #parameters = aruco.DetectorParameters_create()
        parameters =  cv2.aruco.DetectorParameters()
        parameters.adaptiveThreshConstant = 10

        # lists of ids and the corners belonging to each id
        corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)

        geomcomputer = GeomComputer([],[])
        ar = None
        pl = None
        # check if the ids list is not empty
        # if no check is added the code will crash
        if np.all(ids != None):

            # estimate pose of each marker and return the values
            # rvet and tvec-different from camera coefficients
            rvec, tvec ,_ = aruco.estimatePoseSingleMarkers(corners, args.aruco_length, mtx, dist)

            #TODO: for JSON data to be passed to the server
            datum = []
            for i,p in enumerate(tvec):
                dd = {}
                dd["translation"] = {"x" : tvec[i][0][0] , "y" : tvec[i][0][1] , "z" : tvec[i][0][2] }
                dd["rotation"] = {"x" : rvec[i][0][0] , "y" : rvec[i][0][1] , "z" : rvec[i][0][2] }
                dd["id"] = int(ids[i][0])
                datum.append(dd)
                #print(dd['id'],dd['translation'])

            if args.online:
                ws.send(json.dumps(datum))
                #to check whether the server received 
                ws.recv()

            #print(np.linalg.norm(oldtvec - tvec[0][0]))
            geomcomputer = TriangleComputer([p for p in tvec],[r for r in rvec])

            geomcomputer.corners = corners

            oldrvec = rvec[0][0]
            oldtvec = tvec[0][0]
            
            dx, dy, dz = tvec[0][0][0],tvec[0][0][1],tvec[0][0][2]
    
            #(rvec-tvec).any() # get rid of that nasty numpy value array error

            geomcomputer.display(frame,mtx,dist)

            pl = geomcomputer.Perimeter()
            ar = geomcomputer.Area()
            if pl != None:
                print("perimeter :" , pl)
            if ar != None:
                print("area: " , ar)

            # draw a square around the markers
            aruco.drawDetectedMarkers(frame, corners)
            # code to show ids of the marker found
            strg = ''
            for i in range(0, ids.size):
                strg += str(ids[i][0])+', '

            cv2.putText(frame, "Id: " + strg, (0,64), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0),2,cv2.LINE_AA)
        else:
            # code to show 'No Ids' when no markers are found
            cv2.putText(frame, "No Ids", (0,64), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0),2,cv2.LINE_AA)

        if not queues[0].full():
            queues[0].put(frame)

        #(frame,oargs,args,pargs={"area" : 1.0},thresh=0.1):
        feedframe = makeFeedback(frame,args,{"area" : ar})
        if not queues[1].full():
            queues[1].put(feedframe)
    

            


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Aruco Based Happy Geometry')
    parser.add_argument('--online',action="store_true",help="Communicate data through websocket")
    parser.add_argument('--display',default="QT",help="Visualization method")
    parser.add_argument('--aruco_length',default=0.03,help="length of aruco markers",type=float)


    args = parser.parse_args()



    cap = cv2.VideoCapture(0)

    ####---------------------- CALIBRATION ---------------------------
    # termination criteria for the iterative algorithm
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    # checkerboard of size (7 x 6) is used
    objp = np.zeros((6*7,3), np.float32)
    objp[:,:2] = np.mgrid[0:7,0:6].T.reshape(-1,2)

    # arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.

    # iterating through all calibration images
    # in the folder
    images = glob.glob('calib_images/checkerboard/*.jpg')

    for fname in images:
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

        # find the chess board (calibration pattern) corners
        ret, corners = cv2.findChessboardCorners(gray, (7,6),None)

        # if calibration pattern is found, add object points,
        # image points (after refining them)
        if ret == True:
            objpoints.append(objp)

            # Refine the corners of the detected corners
            corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
            imgpoints.append(corners2)

            # Draw and display the corners
            img = cv2.drawChessboardCorners(img, (7,6), corners2,ret)


    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)




    ###-------------------WEBSOCKET--------------------------------

    ws = websocket.WebSocket()
    if args.online:
        ws.connect("ws://127.0.0.1:5000/echo")



    oldtvec = np.zeros(3)
    oldrvec = np.zeros(3)
    modality = Modality.LINE

    app = QApplication(sys.argv)
    queue = Queue()
    feed_queue = Queue()


    video_thread = VideoThread(queue)
    video_widget = VideoWidget(queue,"Video")
    video_thread.change_pixmap_signal.connect(video_widget.show_frame)
    video_thread.start()

    feed_thread = VideoThread(feed_queue)
    feed_widget = VideoWidget(feed_queue,"Feedback")
    feed_thread.change_pixmap_signal.connect(feed_widget.show_frame)
    feed_thread.start()


    read_frame_process = Process(target=readFrame, args=(args,[queue,feed_queue],))
    read_frame_process.start()

    video_widget.video_thread = video_thread
    #video_widget.show()
    
    feed_widget.video_thread = feed_thread
    #feed_widget.show()
    tab1 = QWidget()
    layout = QHBoxLayout()
    label = QLabel("Fate un triangolo di area 10 cm quadrati")
    font = QFont()
    font.setPointSize(16)  # Change 16 to the desired font size
    label.setFont(font)
    layout.addWidget(label)
    layout.addWidget(video_widget)
    layout.addWidget(feed_widget)
   

    tab1.setLayout(layout)
    tab1.show()
 
    sys.exit(app.exec_())




   




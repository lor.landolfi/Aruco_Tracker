
from flask import Flask, render_template, request, session
from flask_sock import Sock

app = Flask(__name__)

sock = Sock(app)
app.secret_key = 'BAD_SECRET_KEY'


lastdata = []

@app.route('/')
def index():
    return render_template('index.html')


@sock.route('/echo')
def echo(sock):
    while True:
        data = sock.receive()
        session['last_data'] = data
        sock.send(data)
        with open("test.txt", "w") as fo:
            fo.write(data+"\n")


@sock.route('/viz')
def viz(sock):
    while True:
        data = sock.receive()
        with open("test.txt", "r") as fo:
            dsend = fo.read()
        sock.send(dsend)


if __name__ == '__main__':
    app.run()
